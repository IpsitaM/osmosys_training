function charFreq(string) {
    var freqList = {};
    for (var i = 0; i < string.length; i++) {
        value = string[i];
        if (value in freqList) {
            freqList[value]++;
        } else {
            freqList[value] = 1;
        }
    }
    return freqList;
};

console.log(charFreq("aabbabcbdbabdbdbabababcbcbab"));