var str = prompt("Enter comma-separated list of numbers");
var arrayOfNum = str.split(",");

function sumClosestToZero() {
    var min = 0,
        sum = 0,
        x = 0,
        y = 0;
    var flag = true;
    for (var i = 0; i < arrayOfNum.length - 1; i++) {
        for (var j = i + 1; j < arrayOfNum.length; j++) {
            if (!(isNaN(arrayOfNum[i])) && !(isNaN(arrayOfNum[j]))) {
                if (flag) {
                    min = parseInt(arrayOfNum[i]) + parseInt(arrayOfNum[j]);
                    flag = false;
                }
                sum = parseInt(arrayOfNum[i]) + parseInt(arrayOfNum[j]);
                if (sum <= min) {
                    min = sum;
                    x = arrayOfNum[i];
                    y = arrayOfNum[j];
                }
            }
        }
    }
    prompt("(" + x + "," + y + ")");
}
sumClosestToZero();