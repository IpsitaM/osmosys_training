var companies = {
    TCS: {
        revenue: 100000000,
        expenses: {
            salaries: 30,
            rent: 20,
            utilities: 15
        },
        employees: [{
                name: "a",
                age: 21,
                role: "Admin"
            },
            {
                name: "b",
                age: 25,
                role: "Tester"
            },
            {
                name: "c",
                age: 35,
                role: "Programmer"
            }
        ]
    },
    GGK: {
        revenue: 200000000,
        expenses: {
            salaries: 10,
            rent: 10,
            utilities: 20
        },
        employees: [{
                name: "d",
                age: 20,
                role: "Admin"
            },
            {
                name: "e",
                age: 27,
                role: "Tester"
            },
            {
                name: "f",
                age: 38,
                role: "Programmer"
            }
        ]

    },
    Osmosys: {
        revenue: 250000000,
        expenses: {
            salaries: 20,
            rent: 10,
            utilities: 10
        },
        employees: [{
                name: "g",
                age: 22,
                role: "Admin"
            },
            {
                name: "h",
                age: 29,
                role: "Tester"
            },
            {
                name: "i",
                age: 30,
                role: "Programmer"
            }
        ]
    }
}

function getAllEmployees() {
    var allEmployees = [];
    for (var x in companies) {
        companies[x].employees.forEach(element => {
            allEmployees.push(element);
        });
    }
    return allEmployees;
}

function getEmpByAges(ageLimit, isLessThan, employeesList) {
    let employees = [];
    if (isLessThan) {
        employees = employeesList.filter((employee) => employee["age"] <= ageLimit);

    } else {
        employees = employeesList.filter((employee) => employee["age"] > ageLimit);
    }
    return employees;
}

function youngestAndOldest(allEmployees) {
    let sortedEmployees = allEmployees.sort((a, b) => a.age < b.age);
    let youngestEmployee = sortedEmployees[0];
    let oldestEmployee = sortedEmployees[sortedEmployees.length - 1];
    let resultObj = {
        youngestEmployee: youngestEmployee,
        oldestEmployee: oldestEmployee
    };
    return resultObj;
}

function getEmpOnRoleBasis() {
    const allEmployees = getAllEmployees;
    const programmers = allEmployees.filter(e => e.role === "Programmer");
    const admins = allEmployees.filter(e => e.role === "Admin");
    const testers = allEmployees.filter(e => e.role === "Tester");

    programmers.forEach(e => { console.log(`${e.name} is programmer.`) })
    admins.forEach(e => { console.log(`${e.name} is Admin.`) })
    testers.forEach(e => { console.log(`${e.name} is Tester.`) })
}

function getprofitOfAll(companies) {
    let allCompanyExpenses = [];
    let revenue;
    let savings;
    for (cname in companies) {
        let expensePercentage = 0;
        for (expense in companies[cname].expenses) {
            expensePercentage += companies[cname].expenses[expense]
        }
        revenue = companies[cname].revenue;
        savings = (revenue) * ((100 - expensePercentage) / 100);
        allCompanyExpenses.push({
            companyName: cname,
            savings: savings

        });
    }
    return allCompanyExpenses;
}

function companyEarningMost() {
    let profitOfAll = getprofitOfAll();
    let max = 0;
    profitOfAll.forEach(x => {
        if (x[1] > max) {
            max = x[1];
        }
    });

    profitOfAll.forEach(x => {
        if (x[1] === max) {
            console.log(`${x[0]} is earning most profit among all companies`);
        }
    });
}