$(document).ready(() => {
    var images = [
        './images/Triangle.jpg',
        './images/rect.gif',
        './images/square.jpg',
        './images/circle.jpg',
        './images/cloud.jpg',
        './images/tree.jpg',

    ]

    var questions = [
        'Click on a triangle shaped object',
        'Click on a rectangle shaped object',
        'Click on a square shaped object',
        'Click on a circle shaped object',
        'Click on a cloud',
        'Click on a tree'
    ]
    $.each(questions, function() {
        $('#questionsList').append($("<p></p>"));
    });

    function questionPopUp() {
        var orderOfQuestionPopUp = Math.floor(Math.random() * questions.length);
        alert(questions[orderOfQuestionPopUp]);
    }

    function start() {
        $("#imagesList").children(".image").click(function(event) {
            event.preventDefault()
            for (index = 0; index < questions.length; index++) {
                if (orderOfQuestionPopUp === questions[index]) {
                    alert("Correct Image Chosen,Congratulations!!");
                    confirmToPlay();
                } else {
                    continueTrying();
                }
            }
        })
    }

    function confirmToPlay() {
        var confirmation = confirm("Yes,I want to play!!");
        if (confirmation) {
            game();
        } else {
            $('body').html("Thanks for Playing!")
        }
    }

    function continueTrying() {
        alert("Sorry,please try again.");
        start();
    }
    $.each(images, function() {
        $('#imagesList').append('<img class = "image" src="./images/Triangle.jpg">');
        $('#imagesList').append('<img class = "image" src="./images/rect.gif">');
        $('#imagesList').append('<img class = "image" src="./images/square.jpg">');
        $('#imagesList').append('<img class = "image" src="./images/circle.jpg">');
        $('#imagesList').append('<img class = "image" src="./images/cloud.jpg">');
        $('#imagesList').append('<img class = "image" src="./images/tree.jpg">');

    });

    questionPopUp();
});