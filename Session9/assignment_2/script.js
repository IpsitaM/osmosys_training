var numberOfColumn = window.prompt("Enter number of columns (Odd Number) :")

var numberOfRows = (numberOfColumn / 2) + 1

for (let index = 1; index <= numberOfRows; index++) {
    for (let spaces = (numberOfRows - index + 1) * 2; spaces >= 1; spaces--) {
        document.write("&nbsp");
    }
    for (let values = 1; values < index * 2; values++) {
        document.write(values);
    }
    document.writeln("<br>");

}