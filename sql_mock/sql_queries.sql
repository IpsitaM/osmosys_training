create schema sql_queries;
USE res_productions;
 INSERT INTO author_details(author_name,author_dob,description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("William Shakespeare","1616-04-23","William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world's greatest dramatist. He is often called England's national poet.","2020-06-25","2020-07-16",17,12,1 );
 select*from author_details;
 INSERT INTO publisher_details(publisher_name,established,pub_description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Hachett Livre",1826," Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.","2019-06-12","2020-07-14",10,13,1);
INSERT INTO publisher_details(publisher_name,established,pub_description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Harper Collins",1989,"HarperCollins Publishers LLC is one of the world's largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.","2019-06-12","2020-07-14",10,13,1);
INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,year_of_release,rating,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Harry Potter and the chamber of secrets","J.K Rowling","Bloomsbury Publishing","Fantasy Fiction",1998,4.5,"2019-07-19","2020-06-23",15,10,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Callie","Mendes","female","1990-03-20","normal","Skagen 20,Stavanger",4238674186,"2019-06-20","2020-06-15",20,24,1);
select first_name,last_name from user_registration where normal_or_premium="normal";
select first_name,last_name from user_registration where gender="female";
select book_details.book_title as rented, user_registration.first_name,user_registration.last_name
from book_details
join user_registration
on book_details.id=user_registration.id;
select book_details.book_title as liked, user_registration.first_name,user_registration.last_name
from book_details
join user_registration
on book_details.id=user_registration.id;
select book_title from book_details where rating>4;
select MAX(rating) from book_details;
select MIN(rating) from book_details;
select author_name from author_details where author_name like "AR";
select publisher_name from publisher_details where established<2012;
SELECT * FROM book_details WHERE id=(SELECT id, MAX(rating) FROM book_details limit 5);
select book_title from book_details where year_of_release<2018 and year_of_release>2012;
select author_name from author_details;




