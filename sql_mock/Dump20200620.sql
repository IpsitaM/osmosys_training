-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: res_productions
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `author_details`
--

DROP TABLE IF EXISTS `author_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `author_details` (
  `book_id` int NOT NULL AUTO_INCREMENT,
  `author_name` text,
  `author_dob` date DEFAULT NULL,
  `description` text,
  `created_on` date DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `modified_by_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `author_details`
--

LOCK TABLES `author_details` WRITE;
/*!40000 ALTER TABLE `author_details` DISABLE KEYS */;
INSERT INTO `author_details` VALUES (1,'Emily Dickinson','1830-12-10','Emily Elizabeth Dickinson was an American poet. Dickinson was born in Amherst, Massachusetts, into a prominent family with strong ties to its community.','2019-07-25','2020-07-16',17,12,1),(2,'J.K Rowling','1965-07-31','Joanne Rowling, CH, OBE, HonFRSE, FRCPE, FRSL, better known by her pen name J. K. Rowling, is a British author, screenwriter, producer, and philanthropist','2019-07-25','2020-07-16',17,12,1),(3,'Nicholas Sparks','1965-12-31','Nicholas Charles Sparks is an American novelist, screenwriter, and philanthropist. He has published twenty novels and two non-fiction books, all of which have been New York Times bestsellers, with over 100 million copies sold worldwide in more than 50 languages.','2019-07-25','2020-07-16',17,12,1),(4,'Dan Brown','1964-06-22','Daniel Gerhard Brown is an American author best known for his thriller novels, including the Robert Langdon novels Angels & Demons, The Da Vinci Code, The Lost Symbol, Inferno and Origin. His novels are treasure hunts that usually take place over a period of 24 hours.','2019-07-25','2020-07-16',17,12,1),(5,'Cecelia Ahern','1981-09-30','Cecelia Ahern is an Irish novelist known for her works like PS, I Love You, Where Rainbows End and If You Could See Me Now. Born in Dublin, Ahern is now published in nearly fifty countries, and has sold over 25 million copies of her novels worldwide. Two of her books have been adapted as major motion films.','2019-07-25','2020-07-16',17,12,1),(6,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(7,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(8,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(9,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(10,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(11,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(12,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(13,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1),(14,'William Shakespeare','1616-04-23','William Shakespeare was an English poet, playwright, and actor, widely regarded as the greatest writer in the English language and the world\'s greatest dramatist. He is often called England\'s national poet.','2020-06-25','2020-07-16',17,12,1);
/*!40000 ALTER TABLE `author_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `book_details`
--

DROP TABLE IF EXISTS `book_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `book_details` (
  `book_id` int NOT NULL AUTO_INCREMENT,
  `book_title` text,
  `book_author` text,
  `book_publisher` text,
  `book_genre` text,
  `year_of_release` int DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `modified_by_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `book_details`
--

LOCK TABLES `book_details` WRITE;
/*!40000 ALTER TABLE `book_details` DISABLE KEYS */;
INSERT INTO `book_details` VALUES (1,'Call me by your name','Andre Aciman','Farrar, Straus and Giroux','Romace,Drama',2007,5,'2019-07-19','2020-06-23',15,10,1),(2,'Harry Potter and the order of phoenix','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',2003,5,'2019-07-19','2020-06-23',15,10,1),(3,'Harry Potter and the philosppher\'s stone','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1997,5,'2019-07-19','2020-06-23',15,10,1),(4,'Harry Potter and the goblet of fire','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',2000,5,'2019-07-19','2020-06-23',15,10,1),(5,'Harry Potter and the deathly hallows','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',2007,5,'2019-07-19','2020-06-23',15,10,1),(6,'Harry Potter and the chamber of secrets','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1998,5,'2019-07-19','2020-06-23',15,10,1),(7,'Harry Potter and the chamber of secrets','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1998,5,'2019-07-19','2020-06-23',15,10,1),(8,'Harry Potter and the chamber of secrets','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1998,5,'2019-07-19','2020-06-23',15,10,1),(9,'Harry Potter and the chamber of secrets','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1998,5,'2019-07-19','2020-06-23',15,10,1),(10,'Harry Potter and the chamber of secrets','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1998,5,'2019-07-19','2020-06-23',15,10,1),(11,'Harry Potter and the chamber of secrets','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1998,5,'2019-07-19','2020-06-23',15,10,1),(12,'Harry Potter and the chamber of secrets','J.K Rowling','Bloomsbury Publishing','Fantasy Fiction',1998,5,'2019-07-19','2020-06-23',15,10,1);
/*!40000 ALTER TABLE `book_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `publisher_details`
--

DROP TABLE IF EXISTS `publisher_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `publisher_details` (
  `book_id` int NOT NULL AUTO_INCREMENT,
  `publisher_name` text,
  `established` int DEFAULT NULL,
  `pub_description` text,
  `created_on` date DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `modified_by_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `publisher_details`
--

LOCK TABLES `publisher_details` WRITE;
/*!40000 ALTER TABLE `publisher_details` DISABLE KEYS */;
INSERT INTO `publisher_details` VALUES (1,'Penguin Random House',1927,'Random House was founded in 1927 by Bennett Cerf and Donald Klopfer, two years after they acquired the Modern Library imprint from publisher Horace Liveright, which reprints classic works of literature. Cerf is quoted as saying, We just said we were going to publish a few books on the side at random, which suggested the name Random House.','2019-06-12','2020-07-14',10,13,1),(2,'Henry Holt and Company',1866,'Henry Holt and Company is an American book publishing company based in New York City. One of the oldest publishers in the United States, it was founded in 1866 by Henry Holt and Frederick Leypoldt.','2019-06-12','2020-07-14',10,13,1),(3,'Farrar, Straus and Giroux',1946,'Farrar, Straus and Giroux is an American book publishing company, founded in 1946 by Roger Williams Straus Jr. and John C. Farrar. FSG is known for publishing literary books, and its authors have won numerous awards, including Pulitzer Prizes, National Book Awards, and Nobel Peace Prizes.','2019-06-12','2020-07-14',10,13,1),(4,'Bloombury publishing',1986,'Bloomsbury Publishing plc is a British worldwide publishing house of fiction and non-fiction. It is a constituent of the FTSE SmallCap Index. Bloomsbury\'s head office is located in Bloomsbury, an area of the London Borough of Camden.','2019-06-12','2020-07-14',10,13,1),(5,'Hachett Livre',1826,' Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.','2019-06-12','2020-07-14',10,13,1),(6,'Harper Collins',1989,'HarperCollins Publishers LLC is one of the world\'s largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.','2019-06-12','2020-07-14',10,13,1),(7,'Hachett Livre',1826,' Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.','2019-06-12','2020-07-14',10,13,1),(8,'Harper Collins',1989,'HarperCollins Publishers LLC is one of the world\'s largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.','2019-06-12','2020-07-14',10,13,1),(9,'Hachett Livre',1826,' Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.','2019-06-12','2020-07-14',10,13,1),(10,'Harper Collins',1989,'HarperCollins Publishers LLC is one of the world\'s largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.','2019-06-12','2020-07-14',10,13,1),(11,'Hachett Livre',1826,' Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.','2019-06-12','2020-07-14',10,13,1),(12,'Harper Collins',1989,'HarperCollins Publishers LLC is one of the world\'s largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.','2019-06-12','2020-07-14',10,13,1),(13,'Hachett Livre',1826,' Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.','2019-06-12','2020-07-14',10,13,1),(14,'Harper Collins',1989,'HarperCollins Publishers LLC is one of the world\'s largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.','2019-06-12','2020-07-14',10,13,1),(15,'Hachett Livre',1826,' Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.','2019-06-12','2020-07-14',10,13,1),(16,'Harper Collins',1989,'HarperCollins Publishers LLC is one of the world\'s largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.','2019-06-12','2020-07-14',10,13,1),(17,'Hachett Livre',1826,' Founded in 1826 by Louis Hachette as Brédif, the company later became L. Hachette et Compagnie, Librairie Hachette, Hachette SA and Hachette Livre in France.','2019-06-12','2020-07-14',10,13,1),(18,'Harper Collins',1989,'HarperCollins Publishers LLC is one of the world\'s largest publishing companies and is one of the Big Five English-language publishing companies, alongside Penguin Random House, Simon & Schuster, Hachette, and Macmillan.','2019-06-12','2020-07-14',10,13,1);
/*!40000 ALTER TABLE `publisher_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_registration`
--

DROP TABLE IF EXISTS `user_registration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_registration` (
  `id` int NOT NULL AUTO_INCREMENT,
  `first_name` text,
  `last_name` text,
  `gender` text,
  `date_of_birth` date DEFAULT NULL,
  `normal_or_premium` text,
  `address` text,
  `phone_number` bigint DEFAULT NULL,
  `created_on` date DEFAULT NULL,
  `modified_on` date DEFAULT NULL,
  `created_by_id` int DEFAULT NULL,
  `modified_by_id` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_registration`
--

LOCK TABLES `user_registration` WRITE;
/*!40000 ALTER TABLE `user_registration` DISABLE KEYS */;
INSERT INTO `user_registration` VALUES (1,'Richard','Grey','male','1986-03-14','normal','305 - 14th Ave. S. Suite 3B,Seattle',6306342378,'2019-06-20','2020-06-15',23,30,1),(2,'Elio','Perlman','male','1989-05-24','premium','62 Bridgeton Court Upper Darby, PA 19082',2172066249,'2019-06-20','2020-06-15',23,30,1),(3,'Oliver','Green','male','1986-04-04','normal','46 Cottage Street Palm City, FL 34990',7156508066,'2019-06-20','2020-06-15',25,30,1),(4,'Maya','Sullivan','female','1977-03-26','premium','109 Wellington St.Harlingen, TX 78552',2124949385,'2019-06-20','2020-06-15',25,30,1),(5,'Jennifer','Johnson','female','1980-03-14','premium','831 Delaware Ave.Burbank, IL 60459',4067988422,'2019-06-20','2020-06-15',20,24,1),(6,'John','Perry','male','1982-06-17','normal','Keskuskatu 45,Helsinki',3609054355,'2019-06-20','2020-06-15',20,24,1),(7,'Mathew','Brown','male','1976-03-14','premium','ul.Filtrowa 68,Walla',5167830017,'2019-06-20','2020-06-15',20,24,1),(8,'Camilla','Mendes','female','1990-03-20','normal','Skagen 21,Stavanger',4238574186,'2019-06-20','2020-06-15',20,24,1),(9,'Callie','Mendes','female','1990-03-20','normal','Skagen 20,Stavanger',4238674186,'2019-06-20','2020-06-15',20,24,1),(10,'Callie','Mendes','female','1990-03-20','normal','Skagen 20,Stavanger',4238674186,'2019-06-20','2020-06-15',20,24,1),(11,'Callie','Mendes','female','1990-03-20','normal','Skagen 20,Stavanger',4238674186,'2019-06-20','2020-06-15',20,24,1),(12,'Callie','Mendes','female','1990-03-20','normal','Skagen 20,Stavanger',4238674186,'2019-06-20','2020-06-15',20,24,1),(13,'Callie','Mendes','female','1990-03-20','normal','Skagen 20,Stavanger',4238674186,'2019-06-20','2020-06-15',20,24,1),(14,'Callie','Mendes','female','1990-03-20','normal','Skagen 20,Stavanger',4238674186,'2019-06-20','2020-06-15',20,24,1),(15,'Callie','Mendes','female','1990-03-20','normal','Skagen 20,Stavanger',4238674186,'2019-06-20','2020-06-15',20,24,1);
/*!40000 ALTER TABLE `user_registration` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 13:50:29
-- MySQL dump 10.13  Distrib 8.0.20, for Win64 (x86_64)
--
-- Host: localhost    Database: sql_queries
-- ------------------------------------------------------
-- Server version	8.0.20

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 13:50:29
