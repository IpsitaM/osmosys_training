CREATE DATABASE res_productions;
USE res_productions;
CREATE TABLE user_registration(id INTEGER PRIMARY KEY AUTO_INCREMENT,
first_name TEXT,
last_name TEXT,
gender TEXT,
date_of_birth DATE,
normal_or_premium TEXT,
address TEXT,
phone_number BIGINT,
created_on DATE,
modified_on DATE,
created_by_id INTEGER,
modified_by_id INTEGER,
status INTEGER
);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Richard","Grey","male","1986-03-14","normal","305 - 14th Ave. S. Suite 3B,Seattle",6306342378,"2019-06-20","2020-06-15",23,30,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Elio","Perlman","male","1989-05-24","premium","62 Bridgeton Court Upper Darby, PA 19082",2172066249,"2019-06-20","2020-06-15",23,30,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Oliver","Green","male","1986-04-04","normal","46 Cottage Street Palm City, FL 34990",7156508066,"2019-06-20","2020-06-15",25,30,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Maya","Sullivan","female","1977-03-26","premium","109 Wellington St.Harlingen, TX 78552",2124949385,"2019-06-20","2020-06-15",25,30,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Jennifer","Johnson","female","1980-03-14","premium","831 Delaware Ave.Burbank, IL 60459",4067988422,"2019-06-20","2020-06-15",20,24,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("John","Perry","male","1982-06-17","normal","Keskuskatu 45,Helsinki",3609054355,"2019-06-20","2020-06-15",20,24,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Mathew","Brown","male","1976-03-14","premium","ul.Filtrowa 68,Walla",5167830017,"2019-06-20","2020-06-15",20,24,1);
INSERT INTO user_registration(first_name,last_name,gender,date_of_birth,normal_or_premium,address,phone_number,created_on,modified_on,created_by_id,modified_by_id,status) VALUES ("Camilla","Mendes","female","1990-03-20","normal","Skagen 21,Stavanger",4238574186,"2019-06-20","2020-06-15",20,24,1);

CREATE TABLE book_details(id INTEGER PRIMARY KEY AUTO_INCREMENT,
book_title TEXT,
book_author TEXT,
book_publisher TEXT,
 book_genre TEXT,
year_of_release INTEGER,
rating INTEGER,
created_on DATE,
modified_on DATE,
created_by_id INTEGER,
modified_by_id INTEGER,
status INTEGER
);
INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,year_of_release,rating,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Call me by your name","Andre Aciman","Farrar, Straus and Giroux","Romace,Drama",2007,4.5,"2019-07-19","2020-06-23",15,10,1);
INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,year_of_release,rating,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Harry Potter and the order of phoenix","J.K Rowling","Bloomsbury Publishing","Fantasy Fiction",2003,4.5,"2019-07-19","2020-06-23",15,10,1);
INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,year_of_release,rating,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Harry Potter and the philosppher's stone","J.K Rowling","Bloomsbury Publishing","Fantasy Fiction",1997,4.7,"2019-07-19","2020-06-23",15,10,1);
INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,year_of_release,rating,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Harry Potter and the goblet of fire","J.K Rowling","Bloomsbury Publishing","Fantasy Fiction",2000,4.5,"2019-07-19","2020-06-23",15,10,1);
INSERT INTO book_details(book_title,book_author,book_publisher,book_genre,year_of_release,rating,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Harry Potter and the deathly hallows","J.K Rowling","Bloomsbury Publishing","Fantasy Fiction",2007,4.5,"2019-07-19","2020-06-23",15,10,1);


CREATE TABLE author_details(id INTEGER PRIMARY KEY AUTO_INCREMENT,
author_name TEXT,
 author_dob DATE,
description TEXT,
created_on DATE,
modified_on DATE,
created_by_id INTEGER,
modified_by_id INTEGER,
status INTEGER
);
INSERT INTO author_details(author_name,author_dob,description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Emily Dickinson","1830-12-10","Emily Elizabeth Dickinson was an American poet. Dickinson was born in Amherst, Massachusetts, into a prominent family with strong ties to its community.","2019-07-25","2020-07-16",17,12,1 );
INSERT INTO author_details(author_name,author_dob,description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("J.K Rowling","1965-07-31","Joanne Rowling, CH, OBE, HonFRSE, FRCPE, FRSL, better known by her pen name J. K. Rowling, is a British author, screenwriter, producer, and philanthropist","2019-07-25","2020-07-16",17,12,1 );
INSERT INTO author_details(author_name,author_dob,description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Nicholas Sparks","1965-12-31","Nicholas Charles Sparks is an American novelist, screenwriter, and philanthropist. He has published twenty novels and two non-fiction books, all of which have been New York Times bestsellers, with over 100 million copies sold worldwide in more than 50 languages.","2019-07-25","2020-07-16",17,12,1 );
INSERT INTO author_details(author_name,author_dob,description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Dan Brown","1964-06-22","Daniel Gerhard Brown is an American author best known for his thriller novels, including the Robert Langdon novels Angels & Demons, The Da Vinci Code, The Lost Symbol, Inferno and Origin. His novels are treasure hunts that usually take place over a period of 24 hours.","2019-07-25","2020-07-16",17,12,1 );
INSERT INTO author_details(author_name,author_dob,description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Cecelia Ahern","1981-09-30","Cecelia Ahern is an Irish novelist known for her works like PS, I Love You, Where Rainbows End and If You Could See Me Now. Born in Dublin, Ahern is now published in nearly fifty countries, and has sold over 25 million copies of her novels worldwide. Two of her books have been adapted as major motion films.","2019-07-25","2020-07-16",17,12,1 );

CREATE TABLE publisher_details(id INTEGER PRIMARY KEY AUTO_INCREMENT,
publisher_name TEXT,
 established INTEGER,
pub_description TEXT,
created_on DATE,
modified_on DATE,
created_by_id INTEGER,
modified_by_id INTEGER,
status INTEGER);
INSERT INTO publisher_details(publisher_name,established,pub_description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Penguin Random House",1927,"Random House was founded in 1927 by Bennett Cerf and Donald Klopfer, two years after they acquired the Modern Library imprint from publisher Horace Liveright, which reprints classic works of literature. Cerf is quoted as saying, We just said we were going to publish a few books on the side at random, which suggested the name Random House.","2019-06-12","2020-07-14",10,13,1);
INSERT INTO publisher_details(publisher_name,established,pub_description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Henry Holt and Company",1866,"Henry Holt and Company is an American book publishing company based in New York City. One of the oldest publishers in the United States, it was founded in 1866 by Henry Holt and Frederick Leypoldt.","2019-06-12","2020-07-14",10,13,1);
INSERT INTO publisher_details(publisher_name,established,pub_description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Farrar, Straus and Giroux",1946,"Farrar, Straus and Giroux is an American book publishing company, founded in 1946 by Roger Williams Straus Jr. and John C. Farrar. FSG is known for publishing literary books, and its authors have won numerous awards, including Pulitzer Prizes, National Book Awards, and Nobel Peace Prizes.","2019-06-12","2020-07-14",10,13,1);
INSERT INTO publisher_details(publisher_name,established,pub_description,created_on,modified_on,created_by_id,modified_by_id,status) VALUES("Bloombury publishing",1986,"Bloomsbury Publishing plc is a British worldwide publishing house of fiction and non-fiction. It is a constituent of the FTSE SmallCap Index. Bloomsbury's head office is located in Bloomsbury, an area of the London Borough of Camden.","2019-06-12","2020-07-14",10,13,1);

CREATE TABLE user_features(id INTEGER PRIMARY KEY AUTO_INCREMENT,
rented_on DATE,
liked_on DATE,
wishlisted_on DATE,
add_friends_name TEXT,
created_on DATE,
modified_on DATE,
created_by_id INTEGER,
modified_by_id INTEGER,
status INTEGER);
INSERT INTO user_features(rented_on,liked_on,wishlisted_on,add_friends_name) VALUES ("2018-08-14","2018-08-12","2018-08-12","Gareik");
INSERT INTO user_features(rented_on,liked_on,wishlisted_on,add_friends_name) VALUES ("2018-07-10","2018-07-09","2018-07-09","Tom");