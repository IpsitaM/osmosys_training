var friendsBirthday = {
    "friends": [{
            name: "a",
            dob: "05-12-1995"
        },
        {
            name: "b",
            dob: "07-20-1998"
        },
        {
            name: "c",
            dob: "04-20-1990"
        }
    ]
}
localStorage.setItem("friendsBirthday", JSON.stringify(friendsBirthday));

function nextMonthBirthdayList() {
    for (month in friendsBirthday) {
        friendsBirthday.friends.forEach(element => {
            var currentDate = new Date();
            var dateFormat = new Date(element.dob);
            var monthOfCurrentdate = currentDate.getMonth();
            var nextMonth = dateFormat.getMonth();
            if (monthOfCurrentdate - nextMonth === -1) {
                console.log(`It is ${element.name} birthday next month`);
            }
            else{
                console.log("Nobody has birthday next month");
            }
        });
    }
}

localStorage.getItem("friendsBirthday");
nextMonthBirthdayList();

function birthdayOnSunday() {
    for (day in friendsBirthday) {
        friendsBirthday.friends.forEach(element => {

            var dateFormat = new Date(element.dob);
            var dayOfBirthday = dateFormat.getDay();
            if (dayOfBirthday === 0) {
                console.log(`Birthday of ${element.name} is on sunday`);
            }
            else{
                console.log("Nobody has birthday on Sunday");
            }

        });

    }
}
localStorage.getItem("friendsBirthday");
birthdayOnSunday();

function birthdayOnSameDay() {
    for (day in friendsBirthday) {
        friendsBirthday.friends.forEach(element => {
            var dateFormatone = new Date(element.dob);
            if (dateFormatone.getDay === dateFormatone.getDay) {
                console.log(`${element.name} and ${element.name} have birthdays on same day`);
            }
           
        });
    }
}
localStorage.getItem("friendsBirthday");
birthdayOnSameDay();