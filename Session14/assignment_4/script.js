var jimsComputerFiles = [{
        "filename": "flower",
        "type": "jpg"
    },
    {
        "filename": "family-video-clip",
        "type": "mp4"
    },
    {
        "filename": "phone-ringtone",
        "type": "mp3"
    },
    "javascript-exercises.txt",
    "learning-html-basics.rtf",
    {
        "filename": "friend-video-clip",
        "type": "mp4"
    },
    {
        "filename": "resume",
        "type": "docx"
    },
    {
        "filename": "student-report",
        "type": "csv"
    },
    {
        "filename": "sms-ringtone",
        "type": "mp3"
    },
    "html-basics.pdf",
    "dubsmash.mp4",
    "screen-shot.png",
    {
        "filename": "faculty-report",
        "type": "xlsx"
    },
    {
        "filename": "puppy",
        "type": "svg"
    }
]
JSON.stringify(jimsComputerFiles);
const audioReg = new RegExp(/(.*)\.(mp3|mp4)/g);
const documentReg = new RegExp(/(.*)\.(pdf|txt|rtf)/g);
const imageReg = new RegExp(/(.*)\.(png|jpg)/g);
var sortedFiles = [];
var audio = [];
var images = [];
var documents = [];
jimsComputerFiles.forEach(element => {
    if (element.type === "mp3" || element.type === "mp4") {
        audio.push(element.filename);
    } else if (element.type === "jpg" || element.type === "svg") {
        images.push(element.filename);
    } else if (element.type === "docx" || element.type === "xlsx" || element.type === "csv") {
        documents.push(element.filename);
    }
})
var filteredFilesOne = jimsComputerFiles.slice(3, 5);
var filteredFilesTwo = jimsComputerFiles.slice(9, 12);

function matchStrings() {
    for (i = 0; i < filteredFilesTwo.length; i++) {
        if (filteredFilesTwo[i].match(audioReg)) {
            audio.push(filteredFilesTwo[i]);
        } else if (filteredFilesTwo[i].match(documentReg)) {
            documents.push(filteredFilesTwo[i]);
        } else if (filteredFilesTwo[i].match(imageReg)) {
            images.push(filteredFilesTwo[i]);
        }
    }
    for (i = 0; i < filteredFilesOne.length; i++) {
        if (filteredFilesOne[i].match(audioReg)) {
            audio.push(filteredFilesOne[i]);
        } else if (filteredFilesOne[i].match(documentReg)) {
            documents.push(filteredFilesOne[i]);
        } else if (filteredFilesOne[i].match(imageReg)) {
            images.push(filteredFilesOne[i]);
        }
    }

}
matchStrings();
sortedFiles.push(audio);
sortedFiles.push(images);
sortedFiles.push(documents);

console.log(sortedFiles);