$(document).ready(function() {
    var $optionValue = $("div.userBox").children("#selectModel")
    var newOption;
    $("div.userBox").children("#selectMake").change(function() {
            var selectedValue = $('#selectMake').val();
            if (selectedValue == "maruti") {
                newOption = {
                    "Select One Model": 0,
                    "800(1995)": 1995,
                    "Alto": 2000,
                    "WagonR": 2002,
                    "Esteem": 2004,
                    "SX4": 2007
                }

                $optionValue.empty();
                $.each(newOption, function(key, value) {
                    $optionValue
                        .append($("<option></option>")
                            .attr("value", value)
                            .text(key))
                })
            } else if (selectedValue == "tata") {
                newOption = {
                    "Select One Model": 0,
                    "Indica": 2001,
                    "Indigo": 2006,
                    "Safari": 2003,
                    "Sumo": 2001
                }

                $optionValue.empty();
                $.each(newOption, function(key, value) {
                    $optionValue
                        .append($("<option></option>")
                            .attr("value", value)
                            .text(key))
                })
            } else if (selectedValue == "chevrolet") {
                newOption = {
                    "Select One Model": 0,
                    "Beat": 2006,
                    "Travera": 2002,
                    "Spark": 2007
                }

                $optionValue.empty();
                $.each(newOption, function(key, value) {
                    $optionValue
                        .append($("<option></option>")
                            .attr("value", value)
                            .text(key))
                })
            } else if (selectedValue == "toyota") {
                newOption = {
                    "Select One Model": 0,
                    "Camry": 2005,
                    "Etios": 2010,
                    "Corolla": 2003,
                    "Endeavour": 2008
                }
                $optionValue.empty();
                $.each(newOption, function(key, value) {
                    $optionValue
                        .append($("<option></option>")
                            .attr("value", value)
                            .text(key))
                })
            }
        })
        //adding option to the year
    $optionValue.change(function() {
            $optionValue.change(function() {
                var createyear = $(this).val();
                var getNowYear = new Date().getFullYear()
                for (var index = createyear; index <= getNowYear; index++) {
                    $("div.userBox").children("#selectYear")
                        .append($("<option></option>")
                            .attr("value", index)
                            .text(index))
                }

            })

        })
        //Adding Table
    var $user = $("div.userBox")
    $("div.userBox").children("#buttonClick").click(function(event) {
        event.preventDefault()
        var getValue = "<tr>" +
            "<td><input type='checkbox'/></td>" +
            "<td>" + $user.children(".firstname").val() + "</td>" +
            "<td>" + $user.children(".lastname").val() + "</td>" +
            "<td>" + $user.children(".email").val() + "</td>" +
            "<td>" + $user.children(".contact").val() + "</td>" +
            "<td>" + $user.children("#selectMake").val() + "</td>" +
            "<td>" + $user.children("#selectModel").val() + "</td></tr>"

        $("#tableAdd").append(getValue)
    })

    function onCick() {
        $('#buttonClick').on('click', () => {
            alert("Your car has been added to our garage for servicing");
        })
    }
    onCick();

    function validateEmail() {
        var emailReg = new RegExp(/(.*)@\.(.*)/);
        var validate = $('#txtEmail').match(emailReg);
        if (validate) {
            return validate;
        } else {
            alert("Please check the note");
        }
    }
    validateEmail();

})